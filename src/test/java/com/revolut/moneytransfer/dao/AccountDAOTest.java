package com.revolut.moneytransfer.dao;

import static junit.framework.TestCase.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.BeforeClass;
import org.junit.Test;

import com.revolut.moneytransfer.dao.impl.AccountDaoImpl;
import com.revolut.moneytransfer.exception.MoneyTransferException;
import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.utils.DBUtils;

public class AccountDAOTest {
	
	private AccountDao accountDAO = new AccountDaoImpl(); 

	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from
		// src/test/resources/demo.sql
		DBUtils.populateTestData();
	}

	@Test
	public void testGetAccountById() throws MoneyTransferException {
		Account account = accountDAO.getAccountById(1L);
		assertTrue(account.getUserName().equals("John"));
	}

	@Test
	public void testGetNonExistingAccById() throws MoneyTransferException {
		Account account = accountDAO.getAccountById(100L);
		assertTrue(account == null);
	}

	@Test
	public void testCreateAccount() throws MoneyTransferException {
		BigDecimal balance = new BigDecimal(10).setScale(4, RoundingMode.HALF_EVEN);
		Account a = new Account("test2", balance, "CNY");
		long aid = accountDAO.createAccount(a);
		Account afterCreation = accountDAO.getAccountById(aid);
		assertTrue(afterCreation.getUserName().equals("test2"));
		assertTrue(afterCreation.getCurrency().equals("CNY"));
		assertTrue(afterCreation.getBalance().equals(balance));
	}

}