package com.revolut.moneytransfer.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;

public class DatabaseDao {

	private static final String h2_driver = "org.h2.Driver";
	private static final String h2_connection_url = "jdbc:h2:mem:moneyapp;DB_CLOSE_DELAY=-1";
	private static final String h2_user = "sa";
	private static final String h2_password = "sa";

	// init: load driver
	public DatabaseDao() {
		DbUtils.loadDriver(h2_driver);
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(h2_connection_url, h2_user, h2_password);

	}
}