package com.revolut.moneytransfer.dao;

import com.revolut.moneytransfer.exception.MoneyTransferException;
import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.model.Transaction;

public interface AccountDao {

	Account getAccountById(long accountId) throws MoneyTransferException;
    long createAccount(Account account) throws MoneyTransferException;
    int transferAccountBalance(Transaction transaction) throws MoneyTransferException;
}
