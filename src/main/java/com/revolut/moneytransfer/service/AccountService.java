package com.revolut.moneytransfer.service;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.revolut.moneytransfer.dao.AccountDao;
import com.revolut.moneytransfer.dao.impl.AccountDaoImpl;
import com.revolut.moneytransfer.exception.MoneyTransferException;
import com.revolut.moneytransfer.model.Account;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {

	private final AccountDao accountDao = new AccountDaoImpl();
  
    /**
     * Find by account id
     * @param accountId
     * @return
     * @throws MoneyTransferException
     */
    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) throws MoneyTransferException {
        return accountDao.getAccountById(accountId);
    }
    
    /**
     * Create Account
     * @param account
     * @return
     * @throws MoneyTransferException
     */
    @PUT
    @Path("/create")
    public Account createAccount(Account account) throws MoneyTransferException {
        final long accountId = accountDao.createAccount(account);
        return accountDao.getAccountById(accountId);
    }
}
