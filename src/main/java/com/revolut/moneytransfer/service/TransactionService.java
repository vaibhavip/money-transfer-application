package com.revolut.moneytransfer.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.revolut.moneytransfer.dao.AccountDao;
import com.revolut.moneytransfer.dao.impl.AccountDaoImpl;
import com.revolut.moneytransfer.exception.MoneyTransferException;
import com.revolut.moneytransfer.model.Transaction;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionService {

	private final AccountDao accountDao = new AccountDaoImpl();
	
	/**
	 * Transfer fund between two accounts.
	 * @param transaction
	 * @return
	 * @throws MoneyTransferException
	 */
	@POST
	public Response transferFund(Transaction transaction) throws MoneyTransferException {

		int updateCount = accountDao.transferAccountBalance(transaction);
		if (updateCount == 2) {
			return Response.status(Response.Status.OK).build();
		} else {
			// transaction failed
			throw new WebApplicationException("Transaction failed", Response.Status.BAD_REQUEST);
		}

	}
}