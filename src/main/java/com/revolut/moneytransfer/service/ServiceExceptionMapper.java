package com.revolut.moneytransfer.service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.revolut.moneytransfer.exception.MoneyTransferException;

@Provider
public class ServiceExceptionMapper implements ExceptionMapper<MoneyTransferException> {
	private static Logger log = Logger.getLogger(ServiceExceptionMapper.class);

	public ServiceExceptionMapper() {
	}

	public Response toResponse(MoneyTransferException daoException) {
		if (log.isDebugEnabled()) {
			log.debug("Mapping exception to Response....");
		}
		
		// return internal server error for DAO exceptions
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(daoException.getMessage()).type(MediaType.APPLICATION_JSON).build();
	}

}