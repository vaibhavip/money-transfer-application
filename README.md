# Money transfer application

A Java RESTful API for money transfers between users accounts

### Technologies
- JAX-RS API
- H2 in memory database
- Log4j
- Jetty Container (for Test and Demo app)
- Apache HTTP Client


### How to run
```sh
mvn exec:java
```

- http://localhost:8080/account/1
- http://localhost:8080/account/2
- http://localhost:8080/account/create
- http://localhost:8080/transaction


### Available Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /account/{accountId} | get account by accountId |  
| PUT | /account/create | create a new account |
| POST | /transaction | perform transaction between 2 user accounts | 

### Http Status
- 200 OK: The request has succeeded
- 400 Bad Request: The request could not be understood by the server 
- 404 Not Found: The requested resource cannot be found
- 500 Internal Server Error: The server encountered an unexpected condition 

### Sample JSON for Transaction

#### User Transaction:
```sh
{  
   "currency":"EUR",
   "amount":5.0000,
   "fromAccountId":1,
   "toAccountId":2
}
```